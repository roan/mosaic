# mosaic

TUI based helper for mosaic crochet pattern design.  

It is intended to be used to help designing, saving and following mosaic crochet
base patterns. Given its particularity, there are rather strict rules of design,
so the program takes care to make impossible invalid design choices, as well as
it generates two-colour base stripes canvas.

You can share generated patterns with your fellow textile-makers by just sending
them the pattern file (located in ~/.mosaic/).

TODO:  
- highlight mode that helps to follow a particular line.

#### Compilation

Please get the code with `git clone https://codeberg.org/roan/mosaic.git`  
`cd mosaic`  
`gcc -lncurses mosaic.c -o mosaic.out`  

Run:  
`./mosaic.out`
